if CommandLine.arguments.count != 2 {
    print("Usage: hello NAME")
} else {
    print("Hello, \(CommandLine.arguments[1])!")
}

// Inspirated by:
// https://swift.org/getting-started
// 2020-08